package main

import (
	"bitbucket.org/stuartcarnie/elevator/sim"
	"bufio"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"
)

func main() {
	l, err := net.Listen("tcp", ":9876")
	if err != nil {
		panic(fmt.Sprintf("unable to listen on port 9876: %s", err))
	}

	fmt.Println("elevator sim listening on port 9876")
	for {

		c, err := l.Accept()
		if err != nil {
			panic("failed to accept socket")
		}
		r := bufio.NewReader(c)

		var ecs sim.ElevatorControlSystem = sim.NewElevatorControlSystem(1)
		fmt.Fprintf(c, "\nWelcome to ECS\n")
		fmt.Fprintf(c, "type help for a list of commands\n")

		done := false
		var last string
		for !done {
			fmt.Fprintf(c, "> ")
			l, _, err := r.ReadLine()
			if err != nil {
				panic("error reading line")
			}

			if len(l) == 0 {
				l = []byte(last)
			} else {
				last = string(l)
			}

			toks := strings.Split(string(l), " ")
			tl := len(toks)
			var cmd string
			if tl > 0 {
				cmd = toks[0]
			}
			switch strings.ToLower(cmd) {
			case "quit":
				fmt.Fprintf(c, "bye\n")
				done = true

			case "new":
				if tl != 2 {
					fmt.Fprintf(c, "invalid new command")
					continue
				}
				num, err := strconv.Atoi(toks[1])
				if err != nil || (num < 1 || num > 16) {
					fmt.Fprintf(c, "invalid count 1 - 16\n")
					continue
				}
				ecs = sim.NewElevatorControlSystem(num)
				fmt.Fprintf(c, "restarting sim with %d elevators\n", num)

			case "step":
				iter := 1
				if tl == 2 {
					iter, err = strconv.Atoi(toks[1])
					if err != nil || iter > 100 {
						fmt.Fprintf(c, "invalid count 1 - 100\n")
						continue
					}
				} else if tl != 1 {
					fmt.Fprintf(c, "invalid step command\n")
					continue
				}
				fmt.Fprintf(c, "stepping %d time(s)\n", iter)
				for i := 0; i < iter; i++ {
					ecs.Step()
				}

			case "call":
				if tl != 3 {
					fmt.Fprintf(c, "use 'call FLOOR DIR'\n")
					continue
				}
				f, err := strconv.Atoi(toks[1])
				if err != nil || !ecs.IsValidFloor(f) {
					fmt.Fprintf(c, "invalid floor, use 1 - %d\n", sim.Floors)
					continue
				}

				var dir sim.Direction
				d := strings.ToLower(toks[2])
				switch d {
				case "u":
					dir = sim.DirectionUp
				case "d":
					dir = sim.DirectionDown
				default:
					fmt.Fprintf(c, "invalid DIR, use u or d for up or down\n")
					continue
				}

				id := ecs.Call(f, dir)
				fmt.Fprintf(c, "elevator %d is answering your %s call from %d\n", id, d, f)

			case "status":
				if tl != 2 {
					fmt.Fprintf(c, "use 'status ID'\n")
					continue
				}
				id, err := strconv.Atoi(toks[1])
				if err != nil || !ecs.IsValidElevator(id) {
					fmt.Fprintf(c, "invalid elevator id, use 0 - %d\n", ecs.ElevatorCount()-1)
					continue
				}
				printStatus(c, ecs.Status(id))

			case "help":
				fmt.Fprintf(c, `
Command list:

  step [COUNT]    - step the simulation [count] times; defaults to 1

  status ID       - return the status of the elevator with the
                    specified ID

  new COUNT       - restart sim with COUNT elevators

  call FLOOR DIR  - queue a DIR call request from the specified FLOOR
                      DIR = u|d for up|down

  q ID FLOOR      - queue a floor for elevator ID [not done]
`)
			}
		}
		c.Close()
	}
}

func printStatus(w io.Writer, e *sim.Elevator) {
	fmt.Fprintf(w, "\nElevator %d\n", e.Id)
	fmt.Fprintf(w, "  State:         %s\n", e.State)
	fmt.Fprintf(w, "  Current Floor: %d\n", e.CurrentFloor)
	fmt.Fprintf(w, "  Target Floor:  %d\n", e.TargetFloor)
	fmt.Fprintf(w, "  Direction:     %s\n", e.Dir)
	fmt.Fprintf(w, "  Run List:      %v\n", e.CurrentRun)
	fmt.Fprintf(w, "  Goal List:     %v\n", e.Goals)
}
