package sim

type Direction int

const (
	DirectionDown Direction = -1
	DirectionNone           = 0
	DirectionUp             = 1
)

func (d Direction) String() string {
	switch d {
	case DirectionDown:
		return "Down"

	case DirectionNone:
		return "None"

	case DirectionUp:
		return "Up"

	default:
		return "Invalid"
	}
}

type ElevatorControlSystem interface {
	Status(id int) *Elevator
	QueueFloor(id, targetFloor int)
	Call(pickupFloor int, dir Direction) int
	Step()
	IsValidFloor(floor int) bool
	IsValidElevator(id int) bool
	ElevatorCount() int
}

type ecs struct {
	elevators []*Elevator
}

// Floors specifies the number of floors handled by the system
const Floors = 12

// NewElevatorControlSystem creates a new control system
// with num specifying the number of elevators to simulate
func NewElevatorControlSystem(num int) ElevatorControlSystem {
	ecs := new(ecs)

	ecs.elevators = make([]*Elevator, num)
	for i := 0; i < num; i++ {
		ecs.elevators[i] = newElevator(i)
	}

	return ecs
}

func (ecs *ecs) Status(id int) *Elevator {
	if !ecs.IsValidElevator(id) {
		panic("invalid elevator id")
	}

	return ecs.elevators[id]
}

func (ecs *ecs) QueueFloor(id, targetFloor int) {
	if !ecs.IsValidElevator(id) {
		panic("invalid elevator id")
	}

	if !ecs.IsValidFloor(targetFloor) {
		panic("invalid floor")
	}

	ecs.elevators[id].addFloor(targetFloor)
}

func (ecs *ecs) Call(pickupFloor int, dir Direction) int {
	if !ecs.IsValidFloor(pickupFloor) {
		panic("invalid floor")
	}

	var e *Elevator
	d := Floors + 1

	// try to pick an elevator going in the same direction
	for _, i := range ecs.elevators {
		j := i.distanceToFloor(pickupFloor)
		if j > 0 && j < d {
			e = i
			d = j
		}
	}

	if e == nil {
		// pick the one that has the least amount of work
		e = ecs.elevators[0]

		for _, i := range ecs.elevators {
			if i.floorsRemaining() < e.floorsRemaining() {
				e = i
			}
		}
	}

	e.addCall(Call{pickupFloor, dir})
	return e.Id
}

// Step steps the simulation by a single unit of time
func (ecs *ecs) Step() {
	for _, e := range ecs.elevators {
		e.update()
	}
}

// IsValidElevator returns true when the specified id
// is valid
func (ecs *ecs) IsValidElevator(id int) bool {
	return id >= 0 && id < len(ecs.elevators)
}

// IsValidFloor returns true when the specified floor
// is valid
func (ecs *ecs) IsValidFloor(floor int) bool {
	return floor >= 1 && floor <= Floors
}

func (ecs *ecs) ElevatorCount() int {
	return len(ecs.elevators)
}
