package sim

import (
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func sign(n int) int {
	switch {
	case n < 0:
		return -1
	case n > 0:
		return 1
	default:
		return 0
	}
}

func mc(f int) Call {
	return Call{f, DirectionNone}
}

func makeGoals(f []int) []Call {
	r := make([]Call, len(f))
	for i, j := range f {
		r[i] = Call{j, DirectionNone}
	}
	return r
}

func TestElevator(t *testing.T) {
	Convey("tests", t, func() {
		Convey("buildRun tests", func() {
			e := newElevator(1)

			Convey("does generate run for all floors, going up", func() {
				e.Goals = makeGoals([]int{6, 1, 5, 2, 4, 3})
				e.CurrentFloor = 1
				e.buildRun()

				So(e.CurrentRun, ShouldResemble, []int{1, 2, 3, 4, 5, 6})
				So(e.Goals, ShouldBeEmpty)
				So(e.Dir, ShouldEqual, DirectionUp)
			})

			Convey("does generate run for some floors, going up", func() {
				e.Goals = makeGoals([]int{6, 1, 5, 2, 4, 3})
				e.CurrentFloor = 3
				e.buildRun()

				So(e.CurrentRun, ShouldResemble, []int{3, 4, 5, 6})
				So(e.Goals, ShouldResemble, makeGoals([]int{1, 2}))
				So(e.Dir, ShouldEqual, DirectionUp)
			})

			Convey("does generate run for some floors, going down", func() {
				e.Goals = makeGoals([]int{1, 6, 5, 2, 4, 3})
				e.CurrentFloor = 3
				e.buildRun()

				So(e.CurrentRun, ShouldResemble, []int{3, 2, 1})
				So(e.Goals, ShouldResemble, makeGoals([]int{6, 5, 4}))
				So(e.Dir, ShouldEqual, DirectionDown)
			})

			Convey("does go straight to floor if call request", func() {
				e.Goals = []Call{{6, DirectionDown}, mc(5)}
				e.CurrentFloor = 1
				e.buildRun()

				So(e.CurrentRun, ShouldResemble, []int{6})
				So(e.Goals, ShouldResemble, makeGoals([]int{5}))
				So(e.Dir, ShouldEqual, DirectionUp)
			})
		})

		Convey("update tests", func() {
			e := newElevator(1)

			Convey("transitions to NextFloor when has goal", func() {
				e.addFloor(3)
				e.update()

				So(e.State, ShouldEqual, ElevatorNextFloor)
				Convey("transitions to Moving", func() {
					e.update()

					So(e.State, ShouldEqual, ElevatorMoving)
					So(e.TargetFloor, ShouldEqual, 3)
					So(e.CurrentFloor, ShouldEqual, 1)

					Convey("transitions to Landed", func() {
						e.update()
						e.update()
						e.update()

						So(e.State, ShouldEqual, ElevatorLanded)
						So(e.CurrentFloor, ShouldEqual, 3)
					})
				})
			})

			Convey("transitions to NextFloor when has call", func() {
				e.addCall(Call{3, DirectionDown})
				e.update()

				So(e.State, ShouldEqual, ElevatorNextFloor)
			})

		})

		Convey("addFloor tests", func() {
			e := newElevator(1)

			Convey("when going up, does add floor to run list", func() {
				e.Dir = DirectionUp
				e.State = ElevatorMoving
				e.CurrentFloor = 5
				e.TargetFloor = 7
				e.CurrentRun = []int{10}
				e.addFloor(9)

				So(e.CurrentRun, ShouldResemble, []int{9, 10})
			})

			Convey("when going up, does add floor to goal list", func() {
				e.Dir = DirectionUp
				e.State = ElevatorMoving
				e.CurrentFloor = 5
				e.TargetFloor = 7
				e.CurrentRun = []int{10}
				e.addFloor(6)

				So(e.CurrentRun, ShouldResemble, []int{10})
				So(e.Goals, ShouldResemble, makeGoals([]int{6}))
			})

			Convey("when going down, does add floor to run list", func() {
				e.Dir = DirectionDown
				e.State = ElevatorMoving
				e.CurrentFloor = 5
				e.TargetFloor = 3
				e.CurrentRun = []int{1}
				e.addFloor(2)

				So(e.CurrentRun, ShouldResemble, []int{2, 1})
			})

			Convey("when going down, does add floor to goal list", func() {
				e.Dir = DirectionDown
				e.State = ElevatorMoving
				e.CurrentFloor = 5
				e.TargetFloor = 3
				e.CurrentRun = []int{1}
				e.addFloor(4)

				So(e.CurrentRun, ShouldResemble, []int{1})
				So(e.Goals, ShouldResemble, makeGoals([]int{4}))
			})
		})

		Convey("distanceToFloor tests", func() {
			e := newElevator(1)

			Convey("when going up, returns negative when floor is below current floor", func() {
				e.CurrentFloor = 5
				e.Dir = DirectionUp
				So(sign(e.distanceToFloor(1)), ShouldEqual, -1)
			})

			Convey("when going up, returns positive when floor is above current floor", func() {
				e.CurrentFloor = 5
				e.Dir = DirectionUp
				So(sign(e.distanceToFloor(6)), ShouldEqual, 1)
			})

			Convey("when going down, returns negative when floor is above current floor", func() {
				e.CurrentFloor = 5
				e.Dir = DirectionDown
				So(sign(e.distanceToFloor(6)), ShouldEqual, -1)
			})

			Convey("when going down, returns positive when floor is below current floor", func() {
				e.CurrentFloor = 5
				e.Dir = DirectionDown
				So(sign(e.distanceToFloor(3)), ShouldEqual, 1)
			})
		})
	})
}
