package sim

import (
	"fmt"
	"sort"
)

type Elevator struct {
	Id           int
	CurrentFloor int
	TargetFloor  int
	Dir          Direction
	CurrentRun   []int // represents a sorted list of floors to land on
	State        ElevatorState
	Goals        []Call
}

type ElevatorState int

const (
	ElevatorWaiting ElevatorState = iota
	ElevatorMoving
	ElevatorLanded
	ElevatorNextFloor
)

func (s ElevatorState) String() string {
	switch s {
	case ElevatorWaiting:
		return "Waiting"

	case ElevatorMoving:
		return "Moving"

	case ElevatorLanded:
		return "Landed"

	case ElevatorNextFloor:
		return "NextFloor"

	default:
		return "Invalid"
	}
}

// call represents a call request
type Call struct {
	floor int
	dir   Direction
}

func (c Call) String() string {
	return fmt.Sprintf("floor: %d, call direction: %s", c.floor, c.dir)
}

func newElevator(id int) *Elevator {
	return &Elevator{
		Id:           id,
		CurrentFloor: 1,
		TargetFloor:  1,
		Dir:          DirectionNone,
		State:        ElevatorWaiting,
	}
}

func (e *Elevator) update() {
	switch e.State {
	case ElevatorWaiting:
		switch {
		case e.hasGoals():
			e.State = ElevatorNextFloor
		}

	case ElevatorMoving:
		if e.CurrentFloor == e.TargetFloor {
			e.State = ElevatorLanded
			break
		}
		e.CurrentFloor += int(e.Dir)

	case ElevatorLanded:
		e.State = ElevatorNextFloor

	case ElevatorNextFloor:
		if len(e.CurrentRun) == 0 {
			if !e.hasGoals() {
				// no more work
				e.State = ElevatorWaiting
				e.Dir = DirectionNone
				break
			}
			e.buildRun()
			e.State = ElevatorMoving
		}
		e.TargetFloor = e.CurrentRun[0]
		if len(e.CurrentRun) == 1 {
			e.CurrentRun = []int{}
		} else {
			e.CurrentRun = e.CurrentRun[1:]
		}
	}
}

func directionToFloor(from, to int) Direction {
	if to < from {
		return DirectionDown
	} else if to > from {
		return DirectionUp
	}

	return DirectionNone
}

// distanceToFloor will return number of floors between the CurrentFloor
// and the target floor. A positive result indicates the target floor is in the
// same direction as the elevator is currently headed
func (e *Elevator) distanceToFloor(to int) int {
	return (to - e.CurrentFloor) * int(e.Dir)
}

func (e *Elevator) buildRun() {
	ng := e.nextGoal()
	e.Dir = directionToFloor(e.CurrentFloor, ng.floor)
	if ng.dir != DirectionNone {
		// processing a call after finishing a run; do not add anyone to the run and go straight to the call floor
		e.CurrentRun = []int{ng.floor}
		return
	}

	// build a new list floors for the run in the current direction
	run := []int{ng.floor}
	goals := make([]Call, 0)

	for _, f := range e.Goals {
		// only calls in the same direction or queued floors are okay
		ok := f.dir == DirectionNone || f.dir == e.Dir
		dir := directionToFloor(e.CurrentFloor, f.floor)
		if ok && (dir == e.Dir || dir == DirectionNone) {
			run = append(run, f.floor)
		} else {
			goals = append(goals, f)
		}
	}

	if e.Dir == DirectionUp {
		sort.Ints(run)
	} else {
		sort.Sort(sort.Reverse(sort.IntSlice(run)))
	}
	e.Goals = goals
	e.CurrentRun = run
}

func (e *Elevator) nextGoal() Call {
	if !e.hasGoals() {
		return Call{-1, DirectionNone}
	}

	f := e.Goals[0]
	e.Goals = e.Goals[1:]
	return f
}

func (e *Elevator) addCall(g Call) {
	e.Goals = append(e.Goals, g)
}

func (e *Elevator) addFloor(f int) {
	if f == e.CurrentFloor || f == e.TargetFloor {
		// we're already here or we're going there
		return
	}

	dir := directionToFloor(e.CurrentFloor, f)
	ok := (dir == DirectionDown && f < e.TargetFloor) ||
		(dir == DirectionUp && f > e.TargetFloor)
	if ok && dir == e.Dir {
		// insert into existing Run list
		var run []int = make([]int, len(e.CurrentRun))
		copy(run, e.CurrentRun)
		run = append(run, f)
		if e.Dir == DirectionDown {
			sort.Sort(sort.Reverse(sort.IntSlice(run)))
		} else {
			sort.Ints(run)
		}

		e.CurrentRun = run
	} else {
		e.Goals = append(e.Goals, Call{f, DirectionNone})
	}
}

func (e *Elevator) hasGoals() bool {
	return len(e.Goals) > 0
}

func (e *Elevator) floorsRemaining() int {
	return len(e.CurrentRun) + len(e.Goals)
}
