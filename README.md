Elevator Sim
============

Simulates 1 - 16 elevators, providing a simple telnet interface to manipulate
the state of the Elevator simulator

Description
-----------

### ElevatorControlSystem
The ElevatorControlSystem (ECS) is the interface for manipulating and
querying the simulation, such as obtaining the status of an elevator
or queueing a call request.

When a call request is made, the ECS is responsible for selecting
the most appropriate elevator to handle the requests. The current
algorithm is quite simple.

**Phase One**  
First, the ECS attempts to find the closest elevator moving
in the direction of the call request

**Phase Two**  
If Phase One fails, the ECS will select the elevator
with the least amount of work, so the caller is not waiting too
long.

### Elevator
The `Elevator` operates as a simple state machine, and represents the state
of a single elevator. It is responsible for moving the elevator from
floor to floor and handling goals. It presents a simple API to the ECS
for adding floors, which would happen inside the elevator control panel
or adding calls, which would happen when someone is waiting on a given floor.
A **run** is considered a list of target floors in a single direction.

#### Elevator.addFloor
When an **addFloor** request is made, the elevator applies some
simple rules to ensure someone cannot hijack the current run. If the
requested floor is suitable to be added to the current run, it will be
inserted into the `Elevator.CurrentRun` list at the correct position;
otherwise, it will be added to the end of the `Elevator.Goals` FIFO list.

#### Elevator.addCall
When an **addCall** request is made, the floor is simply added to
the end of the Elevator.Goals FIFO list.

#### Elevator.update
The update API is called by the ECS Step to evaluate the current state and
potentially transition to a new state.

The Elevator has the following states:

* `Waiting`: checks to see if there is any work to do
* `Moving`: moving towards the `TargetFloor`
* `Landed`: has landed at the `TargetFloor`
* `NextFloor`: calculates the next `TargetFloor` or transitions back to
  `Waiting`

#### NextFloor
`NextFloor` is the most complex state and warrants further discussion.
The `Elevator.CurrentRun` field is a list of the target floors for
the current run, sorted in ascending order if the `Elevator.Dir`is `Up` or
descending if the direction is `Down`. If this list is not empty, the
target floor will be the front of this list. If this list is empty
and items remain in the `Goals` queue, **buildRun** will be called.

#### Elevator.buildRun
The **buildRun** API populates the `CurrentRun` field from the `Goals`
queue. The first item from the `Goals` queue (`gf`) is used to build a full
run and ultimately decides the direction for the run. When building the
`CurrentRun` list:

* If `gf` is a call request (i.e. has an Up or Down direction), the run will
  only contain this floor.
* Otherwise, pull all the floors from the `Goals` queue that are in the same
  direction as `gf` and after the elevator's current floor.

Prerequisites
-------------
Google Go versions after 1.3.x must be installed.

Building / Installing
---------------------
    $ go get bitbucket.org/stuartcarnie/elevator

Running
-------
    $ cd $GOPATH/bin
    $ ./elevator

    $ telnet localhost 9876

Once connected via telnet, enter `help` to list the available commands
